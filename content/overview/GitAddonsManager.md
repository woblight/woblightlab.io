+++
showonlyimage = false
draft = false
date = "2019-10-20"
image = "img/gam.png"
title = "GitAddonsManager"
weight = 0
categories = ["Tools"]
tags = ["tool", "git", "github", "world of warcraft", "classic", "retail", "gitlab", "addons manager"]
+++

Downloads and installs addons from git repositories (GitLab/GitHub/...).

### <div style="text-align: right"> [Download for Windows x64](https://gitlab.com/woblight/GitAddonsManager/-/jobs/artifacts/master/download?job=Win64) </div>
### <div style="text-align: right"> [Download for Linux (flatpak)](https://gitlab.com/woblight/flatpak-repo/-/raw/master/GitAddonsManager.flatpakref?ref_type=heads&inline=false) </div>

<!--more-->

[<div style="text-align: right">Gitlab</div>](https://gitlab.com/woblight/GitAddonsManager)

![GitAddonsManager](/img/gam.png)

## What does it do?

- Download and update addons from git repositories such us GitLab and GitHub. It doesn't require login.
- Repair addons files.
- Delete addons.



## What does it ***NOT*** do?

- It does not search for addons.
- It does not detect existing addons which are not downloaded though a git client (they're missing the .git folder).
- It does not handle addons replaying on `.pkgmeta` files for packing.
