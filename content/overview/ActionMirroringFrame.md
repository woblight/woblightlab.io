+++
showonlyimage = false
draft = false
image = "img/amf2.png"
date = "2019-10-20"
title = "ActionMirroringFrame"
weight = 1
categories = ["Addons", "Streaming"]
tags = ["addon", "actions", "world of warcraft", "classic", "retail"]
+++

ActionMirroringFrame displays the currently attempted actions in a fixed position, with some informations (out of range, missing power, cooldown...).

### <div style="text-align: right"> [Download](https://gitlab.com/woblight/actionmirroringframe/-/archive/master/actionmirroringframe-master.zip)</div>

<!--more-->

[<div style="text-align: right">Gitlab</div>](https://gitlab.com/woblight/actionmirroringframe)

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/amf2.png" caption="Missing mana, out of range">}}
  {{< figure link="/img/amf3.png" caption="Cooldown, current cast">}}
{{< /gallery >}}{{< load-photoswipe >}}

## Streaming

ActionMirroringFrame allow viewer to easier follow your gameplay, displaying informations that usually are very hard to access, such as the spell being usable or in range, its rank, cooldown and the missing power to cast it (if any).

## Other usages

Having the action you're attempting showing up in a fixed place may help you notice faster when something's gone awry, like missing a few points of mana, having a cooldown not ready or a switched action bar.
