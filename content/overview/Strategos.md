+++
showonlyimage = false
draft = false
image = "img/strategos_wsg.png"
date = "2019-10-20"
title = "Strategos"
weight = 1
categories = ["Addons", "PvP"]
tags = ["addon", "battleground", "world of warcraft", "classic", "timers", "map", "Warsong Guch", "Arathi Bain", "Alterac Valley"]
+++

Strategos enhances your battleground experience and gameplay.

### <div style="text-align: right"> [Download](https://gitlab.com/woblight/strategos/-/archive/master/strategos-master.zip)</div>

<!--more-->

[<div style="text-align: right">Gitlab</div>](https://gitlab.com/woblight/strategos)

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/strategos_wsg.png" caption="Warsong Gulch flag carrier frames">}}
  {{< figure link="/img/strategos_av.png" caption="Alterac Valley timers">}}
  {{< figure link="/img/strategos_ab1.png" caption="Arathi Basin Win Projection">}}
  {{< figure link="/img/strategos_ab2.png" caption="Arathi Basin timers">}}
  {{< figure link="/img/strategos_map.png" caption="Health based map pins colors">}}
{{< /gallery >}}{{< load-photoswipe >}}

## Warsong Gulch

Displays flag carriers frames which show information about the name, current health and class of the current flag carriers. They can be clicked to target the flag carrier. **NOTE**: due to API limitations the targetted player can't be changed in combat. When the flag carrier name is greyed out, it can't be targetted by using the frame.

## Alterac Valley

Display the timers of Alterac Valley's nodes in a floating window. You can announce the time in chat by clicking on the timer.
