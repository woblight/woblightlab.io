+++
showonlyimage = false
draft = false
image = "img/hc2.png"
date = "2019-11-17"
title = "HonorCounter"
weight = 1
categories = ["Addons", "PvP"]
tags = ["addon", "honor", "honor graph", "world of warcraft", "classic"]
+++

HonorCounter estimates the honor gained during the day.

### <div style="text-align: right"> [Download](https://gitlab.com/woblight/honorcounter/-/archive/master/honorcounter-master.zip)</div>

<!--more-->

[<div style="text-align: right">Gitlab</div>](https://gitlab.com/woblight/honorcounter)

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/hc1.png" >}}
  {{< figure link="/img/hc2.png" caption="Interrupts and Resists">}}
{{< /gallery >}}{{< load-photoswipe >}}

## Usage

HonorCounter estimates the honor gained during the day. Its windows can be displayed by typing `/hc` or `/honorcounter` in chat.

## Caveats

Each subsequent kill on the same target decreases the honor it gives you by 25% (you don't get honor at all from 5th kill onwards) whithin the day. HonorCounter thus tracks the names of the players you kill the guess the honor, thus will not give accurate values if your game crashes or you had honorkills before activating the addon that day.
