+++
showonlyimage = false
draft = false
image = "img/nitro.jpg"
date = "2020-9-7"
title = "Nitro"
weight = 1
categories = ["Addons", "Streaming"]
tags = ["addon", "speed", "speedometer", "world of warcraft", "classic", "retail"]
+++

Nitro displays your movement speed as percentage of standard player walking speed and yards per second, in a speedometer fashion.

### <div style="text-align: right"> [Download](https://gitlab.com/woblight/nitro/-/archive/master/nitro-master.zip)</div>

<!--more-->

[<div style="text-align: right">Gitlab</div>](https://gitlab.com/woblight/nitro)

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/nitro.jpg" caption="" >}}
{{< /gallery >}}{{< load-photoswipe >}}


## In game commands

`/nitro unlock` to move or resize the frame.  
`/nitro lock` to hide the resize handle.
