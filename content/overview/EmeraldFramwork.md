+++
showonlyimage = false
draft = false
date = "2019-10-20"
title = "EmeraldFramework"
weight = 0
categories = ["Addons", "Addon Development"]
tags = ["addon", "library", "UI", "world of warcraft", "classic", "retail", "addon development"]
+++

EmeraldFramework is an object-oriented UI framework.

### <div style="text-align: right"> [Download](https://gitlab.com/woblight/emeraldframework/-/archive/master/emeraldframework-master.zip)</div>

<!--more-->

[<div style="text-align: right">Gitlab</div>](https://gitlab.com/woblight/emeraldframework)


{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/eframe_options.png" caption="Panel using EmeraldFramwork">}}
{{< /gallery >}}{{< load-photoswipe >}}

## Features
 - Properties
 - Signals & Slots Connections
 - Destructible UI elements with native frames recycling
 - PixelPerfect
 - Layouts
