+++
showonlyimage = false
draft = false
image = "img/stl.gif"
date = "2019-10-20"
title = "SpellTimelines"
weight = 1
categories = ["Addons", "Streaming"]
tags = ["addon", "spellcasts", "world of warcraft", "classic", "retail"]
+++

SpellTimelines displays the timeline of units' recent casts and some additional infomations about them.

### <div style="text-align: right"> [Download](https://gitlab.com/woblight/spelltimelines/-/archive/master/spelltimelines-master.zip)</div>

<!--more-->

[<div style="text-align: right">Gitlab</div>](https://gitlab.com/woblight/spelltimelines)

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/stl.gif" >}}
  {{< figure link="/img/stl2.png" caption="Interrupts and Resists">}}
  {{< figure link="/img/stl3.png" caption="Player Spell Ranks">}}
{{< /gallery >}}{{< load-photoswipe >}}

## Streaming

SpellTimelines allow viewer to easier follow your gameplay by displaying a recent history of spells being cast by youself, as well as other players or units optionally. For each spell it will be displayed the rank, the cast time, whether the cast was interrupted, resisted (resist/miss/parry/dodge/...) or was critical.

## Other usages

SpellTimelines allow you to have a better grasp of what's going around you by monitoring your team members or enemy casts. Compared to the usual castbars, it displays instant casts and abilities, as well as most of their effect miss or resist.
